import React, {useEffect, useState} from 'react';
import Login from './components/Login';
import Dashboard from "./components/Dashboard";
import {getUser} from "./services/UserService";
import Loading from "./components/Loading";

const App: React.FC = () => {
  const [loading, setLoading] = useState(true);
  const [element, setElement] = useState((<Login />));
  useEffect(() => {
      getUser()
          .then(response => {
              setElement(<Dashboard {...response.data}/>);
          }).catch(() => {
          setElement(<Login />);
      }).finally(() => setLoading(false));
  }, []);
  if (loading) return (<Loading />);
  else return element;
};

export default App;