import React, {useEffect, useReducer, useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import {logout} from "../services/UserService";
import {addBook, deleteBook, getBooks} from "../services/BookService";
import {Container, TableHead, withStyles} from "@material-ui/core";
import {DeleteForever} from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import TablePaginationActionsImpl from "./TablePaginationActionsImpl";
import {useCookies} from "react-cookie";
import {User} from "../interfaces/User";
import {Book} from "../interfaces/Book";

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const useStylesTable = makeStyles({
    table: {
        minWidth: 700,
        marginTop: 10,
    },
    textField: {
        width: '20%',
        height: 50,
    },
    button: {
        width: '20%',
        height: 50,
    },
    form: {
        marginBottom: 10,
    }
});

type State = {
    title: string;
    author:  string;
    year: string;
    isButtonDisabled: boolean;
    helperText: string;
    isError: boolean;
    added: boolean;
};

const initialState:State = {
    title: '',
    author: '',
    year: '',
    isButtonDisabled: true,
    helperText: '',
    isError: false,
    added: false
};

type Action = { type: 'setTitle', payload: string }
    | { type: 'setAuthor', payload: string }
    | { type: 'setYear', payload: string }
    | { type: 'setIsButtonDisabled', payload: boolean }
    | { type: 'addingSuccess', payload: string }
    | { type: 'addingFailed', payload: string }
    | { type: 'setIsError', payload: boolean }
    | { type: 'setAdded', payload: boolean };

const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case 'setTitle':
            return {
                ...state,
                title: action.payload
            };
        case 'setAuthor':
            return {
                ...state,
                author: action.payload
            };
        case 'setYear':
            return {
                ...state,
                year: action.payload
            };
        case 'setIsButtonDisabled':
            return {
                ...state,
                isButtonDisabled: action.payload
            };
        case 'addingSuccess':
            return {
                ...state,
                helperText: action.payload,
                isError: false
            };
        case 'addingFailed':
            return {
                ...state,
                helperText: action.payload,
                isError: true
            };
        case 'setIsError':
            return {
                ...state,
                isError: action.payload
            };
        case 'setAdded':
            return {
                ...state,
                added: action.payload
            }
    }
}

const Dashboard = (user: User) => {
    const [books, setBooks] = useState<Book[]>([]);
    const classes = useStylesTable();
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);
    const [state, dispatch] = useReducer(reducer, initialState);
    const [cookie, setCookie, removeCookie] = useCookies();

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, books.length - page * rowsPerPage);

    const handleChangePage = (event: any, newPage: React.SetStateAction<number>) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event: { target: { value: string; }; }) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const deleteRow = (bookId: number) => {
        deleteBook(bookId)
            .then(response => {
                let book: Book = response.data;
                setBooks(books.filter(obj => obj.id !== book.id));
            }).catch(error => {
            console.log(error);
        })
        setPage(0);
    };

    const handleAdding = () => {
        addBook(state.title, state.author, state.year)
            .then(() => {
                dispatch({
                    type: 'addingSuccess',
                    payload: 'Added new book'
                });
                dispatch({
                    type: 'setAdded',
                    payload: true
                });
            }).catch(() => {
            dispatch({
                type: 'addingFailed',
                payload: 'Fields cannot be empty'
            });
        });
    };

    const handleLogout = () => {
        logout()
            .then(() => {
                removeCookie('JSESSIONID');
                window.location.reload(false);
            }).catch(() => {
                console.log('Critical error');
        });
    };

    const handleTitleChange: React.ChangeEventHandler<HTMLInputElement> =
        (event) => {
            dispatch({
                type: 'setTitle',
                payload: event.target.value
            });
        };

    const handleAuthorChange: React.ChangeEventHandler<HTMLInputElement> =
        (event) => {
            dispatch({
                type: 'setAuthor',
                payload: event.target.value
            });
        }

    const handleYearChange: React.ChangeEventHandler<HTMLInputElement> =
        (event) => {
            dispatch({
                type: 'setYear',
                payload: event.target.value
            });
        }

    useEffect(() => {
        getBooks()
            .then(response => {
                setBooks(response.data);
            }).catch(error => {
            console.log(error);
        })
        dispatch({
            type: 'setAdded',
            payload: false
        });
    }, [state.added]);

    useEffect(() => {
        if (state.title.trim() && state.author.trim() && state.year.trim()) {
            dispatch({
                type: 'setIsButtonDisabled',
                payload: false
            });
        } else {
            dispatch({
                type: 'setIsButtonDisabled',
                payload: true
            });
        }
    }, [state.title, state.author, state.year]);

    return (
        <Container fixed>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="custom pagination table">
                    <TableHead>
                        <StyledTableRow>
                            <StyledTableCell>Number</StyledTableCell>
                            <StyledTableCell align="right">Title</StyledTableCell>
                            <StyledTableCell align="right">Author</StyledTableCell>
                            <StyledTableCell align="right">Year</StyledTableCell>
                            {
                                user.role && user.role.toUpperCase() === 'ADMIN' &&
                                (<StyledTableCell align="right">Edit</StyledTableCell>)
                            }
                        </StyledTableRow>
                    </TableHead>
                    <TableBody>
                        {(rowsPerPage > 0
                                ? books.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                : books
                        ).map((row: Book) => (
                            <StyledTableRow key={books.indexOf(row)}>
                                <StyledTableCell component="th" scope="row">
                                    {books.indexOf(row) + 1}
                                </StyledTableCell>
                                <StyledTableCell style={{ width: 160 }} align="right">
                                    {row.title}
                                </StyledTableCell>
                                <StyledTableCell style={{ width: 160 }} align="right">
                                    {row.author}
                                </StyledTableCell>
                                <StyledTableCell style={{ width: 160 }} align="right">
                                    {row.year}
                                </StyledTableCell>
                                {
                                    user.role.toUpperCase() === 'ADMIN' &&
                                    (<StyledTableCell style={{width: 160}} align="right">
                                        <IconButton
                                            onClick={() => {
                                                deleteRow(row.id)
                                            }}
                                            aria-label="delete button"
                                        >
                                            {<DeleteForever/>}
                                        </IconButton>
                                    </StyledTableCell>)
                                }
                            </StyledTableRow>
                        ))}

                        {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                colSpan={3}
                                count={books.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'rows per page' },
                                    native: true,
                                }}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                ActionsComponent={TablePaginationActionsImpl}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
            <form noValidate autoComplete="off" className={classes.form}>
                {
                    user.role && user.role.toUpperCase() === 'ADMIN' &&
                    (<React.Fragment>
                        <TextField
                            error={state.isError}
                            className={classes.textField}
                            id="titleId"
                            label="Title"
                            type="text"
                            color="primary"
                            onChange={handleTitleChange}
                        />
                        <TextField
                            error={state.isError}
                            className={classes.textField}
                            id="authorId"
                            label="Author"
                            type="text"
                            color="primary"
                            onChange={handleAuthorChange}
                        />
                        <TextField
                            error={state.isError}
                            className={classes.textField}
                            id="yearId"
                            label="Year"
                            type="number"
                            color="primary"
                            helperText={state.helperText}
                            onChange={handleYearChange}
                        />
                        <Button
                            className={classes.button}
                            variant="contained"
                            size="large"
                            color="primary"
                            onClick={handleAdding}
                            disabled={state.isButtonDisabled}
                        >
                            Add book
                        </Button>
                    </React.Fragment>)
                }
                <Button
                    className={classes.button}
                    variant="contained"
                    size="large"
                    color="primary"
                    onClick={handleLogout}
                >
                    Logout
                </Button>
            </form>
        </Container>
    );
};

export default Dashboard;