import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from 'react-loader-spinner';
import {createStyles, makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() =>
    createStyles({
        container: {
            position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%)"
        }
    })
);

const Loading = () => {
    const classes = useStyles();
    return <Loader
        className={classes.container}
        type="ThreeDots"
        color="#00BFFF"
        height={100}
        width={100}
    />
}

export default Loading;