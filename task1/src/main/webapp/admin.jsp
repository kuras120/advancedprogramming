<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin</title>
    <!-- CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Author</th>
            <th scope="col">Year</th>
            <th scope="col">-</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${pageContext.request.getAttribute('books')}" var="book" varStatus="loop">
            <tr>
                <th scope="row">${loop.index}</th>
                <td><c:out value="${book.title}"/></td>
                <td><c:out value="${book.author}"/></td>
                <td><c:out value="${book.year}"/></td>
                <td><form action="${pageContext.request.contextPath}/admin" method="get">
                    <input type="hidden" name="title" value="${book.title}"/>
                    <input type="hidden" name="author" value="${book.author}"/>
                    <input type="hidden" name="year" value="${book.year}"/>
                    <button class="btn btn-primary btn-block text-uppercase" type="submit">Delete</button>
                </form></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <form action="${pageContext.request.contextPath}/admin" method="post">
        <div class="row">
            <div class="col">
                <input type="text" id="title" name="title" class="form-control" placeholder="Title" required>
                <label for="title"></label>
            </div>
            <div class="col">
                <input type="text" id="author" name="author" class="form-control" placeholder="Author" required>
                <label for="author"></label>
            </div>
            <div class="col">
                <input type="number" id="year" name="year" class="form-control" placeholder="Year" required>
                <label for="year"></label>
            </div>
            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Submit</button>
        </div>
    </form>
</div>
<!-- jQuery and JS bundle w/ Popper.js -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>
