package filter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import model.ExceptionResponse;
import model.Role;
import model.User;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Base64;

@WebFilter(filterName = "LoginFilter", urlPatterns = {"/dashboard", "/dashboard/*"})
public class CookieFilter implements Filter {

    private Gson gson;

    @Override
    public void init(FilterConfig filterConfig) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        User user = (User) request.getSession().getAttribute("user");
        try {
            if (user == null) {
                throw new Exception("Unauthorized user");
            }
            if (!checkForUserIdCookie(request.getCookies(), user)) {
                throw new Exception("No proper cookie");
            }
            chain.doFilter(req, res);
        } catch (Exception ex) {
            response.setContentType("application/json;charset=UTF-8");
            ExceptionResponse exResponse = new ExceptionResponse();
            exResponse.setData(ex.getLocalizedMessage());
            exResponse.setStatus(401);
            response.setStatus(401);
            gson.toJson(exResponse, response.getWriter());
        }
    }

    private boolean checkForUserIdCookie(Cookie[] cookies, User user) {
        for (var cookie : cookies) {
            if ("userId".equals(cookie.getName())) {
                return new String(Base64.getDecoder().decode(cookie.getValue().getBytes())).equals(user.getLogin());
            }
        }
        return false;
    }
}
