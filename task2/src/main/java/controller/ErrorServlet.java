package controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import model.Response;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "controller.ErrorServlet", urlPatterns = {"/error"})
public class ErrorServlet extends HttpServlet {

    private Gson gson;

    @Override
    public void init() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        String message = "An unexpected error has occurred";
        if (statusCode != null) {
            if (statusCode == 404 || statusCode == 405) {
                message = "The page you are looking for was not found";
            }
        }
        gson.toJson(new Response(message, statusCode != null ? statusCode : 0), response.getWriter());
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    public void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
